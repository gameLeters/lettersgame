__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.conf.urls import patterns, include, url

from views import LandingPage

urlpatterns = patterns('letters.views',
    url(r'^', LandingPage.as_view(), name='landing_page'),
)
