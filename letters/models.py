import random

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Word(models.Model):
    image = models.ImageField(_('word image'), upload_to='word_images', blank=False)
    word = models.CharField(max_length=20, blank=False, unique=True)

    def __unicode__(self):
        return self.word

    class Meta:
        verbose_name = u'word'
        verbose_name_plural = u'words'

    @property
    def shuffle_word(self):
        mixed_word = list(self.word)
        random.shuffle(mixed_word)
        return mixed_word