# -*- coding: utf-8 -*-

import json
import random

from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, DetailView, RedirectView

from .models import Word


class LettersPage(RedirectView):
    permanent = False
    query_string = False
    pattern_name = 'word_page'

    def get_redirect_url(self, *args, **kwargs):
        word_pk_prev = self.request.GET.get('word_pk_prev', 0)
        words_pk_list = Word.objects.exclude(pk=word_pk_prev).values_list('pk', flat=True)
        if words_pk_list:
            word_pk = random.choice(words_pk_list)
            kwargs.update({WordPage.pk_url_kwarg: word_pk})
            return super(LettersPage, self).get_redirect_url(*args, **kwargs)
        else:
            return reverse('landing_page')


class WordPage(DetailView):
    model = Word
    pk_url_kwarg = 'word_pk'
    template_name = 'letters/word_page.html'


def check_word_view(request):
    data = {'is_valid': 0}
    word = request.REQUEST.get('word', None)
    word_pk = request.REQUEST.get('pk', None)
    word_obj = get_object_or_404(Word, pk=word_pk)
    if word.strip() == word_obj.word.strip():
        data['is_valid'] = 1
    return HttpResponse(json.dumps(data), content_type="application/json")
