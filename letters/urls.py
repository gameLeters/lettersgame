__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.conf.urls import patterns, include, url

from .views import LettersPage, WordPage

urlpatterns = patterns('letters.views',
    url(r'^$', LettersPage.as_view(), name='letters_page'),
    url(r'^word/(?P<word_pk>\d+)/', WordPage.as_view(), name='word_page'),
    url(r'^word/check/', 'check_word_view', name='check_word_view'),
)
