__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.conf.urls import patterns, include, url

from .forms import LoginForm

urlpatterns = patterns('django.contrib.auth.views',
    url(r'^logout$',  'logout', {'next_page': '/'}, name='logout'),
    url(r'^login$',  'login', {'template_name': 'accounts/login.html', 'authentication_form': LoginForm}, name='login'),
)
